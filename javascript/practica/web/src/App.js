import React from "react";
import styled from 'styled-components';
import Partners from "./components/Partners";
import Books from "./components/Books";
import Payments from "./components/Payments";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  min-height: calc(100vh);
  margin: 0px;
  padding: 0px;
`
const Slider = styled.div`
  width: 35%;
  min-width: 350px;
`

const Body = styled.div`
  width: 65%;
  min-width: 350px;
`


function App() {
    return (
        <Container>
            <Slider>
                <Partners/>
                <Payments/>
            </Slider>

            <Body>
                <Books/>
            </Body>
        </Container>
    );
}

export default App;
