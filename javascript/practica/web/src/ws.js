const {io} = require('socket.io-client');

export const socket = io("http://localhost", {
    port: 80,
    transports: ['websocket'],
    jsonp: false
});

