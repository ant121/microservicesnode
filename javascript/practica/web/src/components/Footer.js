import React, {useEffect, useState} from 'react';
import {socket} from "../ws";

const Footer = () => {
    const [id, setId] = useState();

    useEffect(() => {
        setTimeout(() => setId(socket.id), 500);
    }, []);

    return (
        <div>
            <h1>{id ? `Estas en linea ${id}` : 'Fuera de linea'}</h1>
        </div>
    );
};

export default Footer;
