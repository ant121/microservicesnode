import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import {socket} from "../ws";

const Container = styled.div`
  position: relative;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-self: center;
  align-content: stretch;
  display: flex;
  width: 100%;
`

const Book = styled.div`
  background-color: #303536;
  margin-bottom: 15px;
  border-radius: 10px;
  padding: 4px;
  display: flex;
  flex-direction: column;
  position: relative;
`

const Portada = styled.img`
  width: 200px;
`

const Icon = styled.img`
  width: 35px;
  height: 35px;
  position: absolute;
  top: -6px;
  right: -5px;
  cursor: pointer;
`

const Title = styled.p`
  color: white;
  font-weight: bold;
  text-align: center;
`

const Button = styled.button`
  background-color: #036e00;
  color: white;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  cursor: pointer;
  opacity: 0.8;
  border-radius: 15px;
  width: 100%;
  height: 45px;
  margin-bottom: 25px;
`

const Books = () => {
    const [data, setData] = useState([]);

    useEffect(() => {

        socket.on('res:books:findAll', ({statusCode, data}) => {
            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit('req:books:findAll', ({enable: true})), 1000)
    }, []);

    const handleDelete = ({id}) => {
        socket.emit('req:books:delete', {id})
    }

    const handleCreate = () => {
        socket.emit('req:books:create', {
            title: 'Ready Player One',
            image: 'https://images-na.ssl-images-amazon.com/images/I/71BjAljTQeL.jpg'
        })
    }

    return (
        <Container>
            <Button onClick={handleCreate}>Crear</Button>
            {
                data.map((book, index) => (
                    <Book key={index}>
                        <Icon src="https://c0.klipartz.com/pngpicture/223/552/gratis-png-eliminar-logotipo-icono-de-boton-eliminar-boton-thumbnail.png"
                              onClick={() => handleDelete({id: book.id})}
                        />
                        <Portada src={book.image}/>
                        <Title>{book.title}</Title>
                    </Book>
                ))
            }

        </Container>
    );
};

export default Books;
