import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import {socket} from "../ws";

const Container = styled.div`
  width: 300px;
  max-width: 300px;
`

const ContainerBody = styled.div`
  height: 350px;
  overflow: scroll;
`

const Name = styled.p`
  color: #333;
`

const Partner = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 15px;
  position: relative;
`

const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const Button = styled.button`
  background-color: #f44336;
  color: white;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  cursor: pointer;
  opacity: 1;
  border-radius: 15px;
  width: 100%;
  height: 45px;
  margin-bottom: 25px;
`

const Icon = styled.img`
  width: 25px;
  height: 25px;
  cursor: pointer;
  margin-left: 10px;
`

const Input = styled.input`
  width: 90%;
  height: 25px;
  margin-left: 15px;
  border-radius: 5px;
`

const Payments = () => {
    const [data, setData] = useState([]);
    const [value, setValue] = useState(0);

    useEffect(() => {

        socket.on('res:payments:findAll', ({statusCode, data, message}) => {
            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit('req:payments:findAll', ({})), 1000)
    }, []);

    const handleCreate = value > 0 ? () => {
        socket.emit('req:payments:create', {socio: value, amount: 300})
    } : null;

    const handleDelete = ({id}) => {
        socket.emit('req:payments:delete', {id})
    }

    const handleInput = (e) => {
        setValue(e.target.value);
    }

    return (
        <Container>
            <Input onChange={handleInput} type='number'/>
            <Button onClick={handleCreate}>Crear</Button>
            <ContainerBody>
                {
                    data.map((payment, index) => (
                            <Partner key={index}>
                                <Body>
                                    <Name>
                                        Cupon de pago {payment.id}
                                    </Name>
                                    <Name>
                                        Socio {payment.socio}
                                    </Name>
                                </Body>
                                <Body>
                                    <Name>
                                        {payment.createdAt}
                                    </Name>
                                    <Icon
                                        src="https://c0.klipartz.com/pngpicture/223/552/gratis-png-eliminar-logotipo-icono-de-boton-eliminar-boton-thumbnail.png"
                                        onClick={() => handleDelete({id: payment.id})}
                                    />
                                </Body>
                            </Partner>
                        )
                    )
                }
            </ContainerBody>
        </Container>
    );
};

export default Payments;
