import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import {socket} from "../ws";

const Container = styled.div`
  width: 300px;
  max-width: 300px;
`

const ContainerBody = styled.div`
  height: 350px;
  overflow: scroll;
`

const Partner = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 15px;
  position: relative;
`

const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const Name = styled.p`
  color: #333;
`

const Phone = styled.p`

`

const Email = styled.p`

`

const Enable = styled.p`
  width: 35px;
  height: 35px;
  border-radius: 50%;
  background-color: ${props => props.enable === true ? 'green' : 'red'};
  opacity: 0.7;
  cursor: pointer;
`

const Button = styled.button`
  background-color: #f44336;
  color: white;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  cursor: pointer;
  opacity: 1;
  border-radius: 15px;
  width: 100%;
  height: 45px;
  margin-bottom: 25px;
`

const Icon = styled.img`
  width: 25px;
  height: 25px;
  cursor: pointer;
  margin-left: 10px;
`

const FeedBack = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const Partners = () => {
    const [data, setData] = useState([]);

    useEffect(() => {

        socket.on('res:partners:findAll', ({statusCode, data}) => {
            if (statusCode === 200) setData(data);
        });

        setTimeout(() => socket.emit('req:partners:findAll', ({})), 1000)
    }, []);

    const handleCreate = () => {
        socket.emit('req:partners:create', {name: 'Anthony Tacuri', phone: '0956221645'})
    }

    const handleDelete = ({id}) => {
        socket.emit('req:partners:delete', {id})
    }

    const handleChange = ({enable, id}) => {
        socket.emit(enable ? 'req:partners:disable' : 'req:partners:enable', {id})
    }

    return (
        <Container>
            <Button onClick={handleCreate}>Crear</Button>
            <ContainerBody>
                {
                    data.map((partner, index) => (
                            <Partner key={index}>
                                <Body>
                                    <Name>
                                        {partner.name}  <small>{partner.id}</small>
                                    </Name>
                                    <Phone>
                                        {partner.phone}
                                    </Phone>
                                </Body>
                                <Body>
                                    <Email>
                                        {partner.email}
                                    </Email>


                                    <FeedBack>
                                        <Enable enable={partner.enable}
                                                onClick={() => handleChange({enable: partner.enable, id: partner.id})}
                                        />
                                        <Icon
                                            src="https://c0.klipartz.com/pngpicture/223/552/gratis-png-eliminar-logotipo-icono-de-boton-eliminar-boton-thumbnail.png"
                                            onClick={() => handleDelete({id: partner.id})}
                                        />
                                    </FeedBack>
                                </Body>
                            </Partner>
                        )
                    )
                }
            </ContainerBody>
        </Container>
    );
};

export default Partners;
