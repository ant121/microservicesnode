const express = require('express');
const http = require('http');
const {Server} = require('socket.io');

const app = express();
const server = http.createServer(app);

const io = new Server(server);

const apiBooks = require('api-book');
const apiPartners = require('api-Partner');
const apiPayments = require('api-Payments');

server.listen(80, () => {
    console.log("Server initialize");
    io.on('connection', socket => {
        console.log("New Conection", socket.id);

        //Libros
        socket.on("req:books:findAll", async ({}) => {

            try {
                console.log("req:books:findAll");
                const {statusCode, message, data} = await apiBooks.FindAll({});

                return io.to(socket.id).emit("res:books:findAll", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }

        });

        socket.on("req:books:findOne", async ({id}) => {
            try {
                console.log("req:books:findOne");
                const {statusCode, message, data} = await apiBooks.FindOne({id});

                return io.to(socket.id).emit("res:books:findOne", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:books:create", async ({title, image,}) => {
            try {
                console.log("req:books:create");
                const {statusCode, message, data} = await apiBooks.Create({title, image,});

                io.to(socket.id).emit('res:books:create', {statusCode, message, data});

                const findAllBooks = await apiBooks.FindAll({});

                return io.to(socket.id).emit("res:books:findAll", findAllBooks);
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:books:update", async ({title, category, sections, image, id}) => {
            try {
                console.log("req:books:update");
                const {statusCode, message, data} = await apiBooks.Update({title, category, sections, image, id});
                return io.to(socket.id).emit("res:books:update", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:books:delete", async ({id}) => {
            try {
                console.log("req:books:delete");
                const {statusCode, message, data} = await apiBooks.Delete({id});

                io.to(socket.id).emit('res:books:delete', {statusCode, message, data});

                const findAllBooks = await apiBooks.FindAll({});

                return io.to(socket.id).emit("res:books:findAll", findAllBooks);
            } catch (e) {
                console.log(e);
            }
        });

        //Socios
        socket.on("req:partners:findAll", async ({enable}) => {

            try {
                console.log("req:partners:findAll");
                const {statusCode, message, data} = await apiPartners.FindAll({enable});

                return io.to(socket.id).emit("res:partners:findAll", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }

        });

        socket.on("req:partners:findOne", async ({id}) => {
            try {
                console.log("req:partners:findOne");
                const {statusCode, message, data} = await apiPartners.FindOne({id});

                return io.to(socket.id).emit("res:partners:findOne", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:partners:create", async ({name, phone}) => {
            try {
                console.log("req:partners:create");
                const {statusCode, message, data} = await apiPartners.Create({name, phone});

                io.to(socket.id).emit("res:partners:create", {statusCode, message, data});

                const findAllPartners = await apiPartners.FindAll({});

                return io.to(socket.id).emit("res:partners:findAll", findAllPartners);
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:partners:update", async ({name, phone, email, age, id}) => {
            try {
                console.log("req:partners:update");
                const {statusCode, message, data} = await apiPartners.Update({name, phone, email, age, id});
                return io.to(socket.id).emit("res:partners:update", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:partners:delete", async ({id}) => {
            try {
                console.log("req:partners:delete");
                const {statusCode, message, data} = await apiPartners.Delete({id});

                io.to(socket.id).emit("res:partners:delete", {statusCode, message, data});

                const findAllPartners = await apiPartners.FindAll({});

                return io.to(socket.id).emit("res:partners:findAll", findAllPartners);
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:partners:enable", async ({id}) => {
            try {
                console.log("req:partners:enable");
                const {statusCode, message, data} = await apiPartners.Enable({id});

                io.to(socket.id).emit("res:partners:enable", {statusCode, message, data});

                const findAllPartners = await apiPartners.FindAll({});

                return io.to(socket.id).emit("res:partners:findAll", findAllPartners);
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:partners:disable", async ({id}) => {
            try {
                console.log("req:partners:disable");
                const {statusCode, message, data} = await apiPartners.Disable({id});

                io.to(socket.id).emit("res:partners:disable", {statusCode, message, data});

                const findAllPartners = await apiPartners.FindAll({});

                return io.to(socket.id).emit("res:partners:findAll", findAllPartners);
            } catch (e) {
                console.log(e);
            }
        });

        //Pagos
        socket.on("req:payments:findAll", async ({}) => {

            try {
                console.log("req:payments:findAll");
                const {statusCode, message, data} = await apiPayments.FindAll({});

                return io.to(socket.id).emit("res:payments:findAll", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }

        });

        socket.on("req:payments:findOne", async ({id}) => {
            try {
                console.log("req:payments:findOne");
                const {statusCode, message, data} = await apiPayments.FindOne({id});

                return io.to(socket.id).emit("res:payments:findOne", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:payments:create", async ({socio, amount}) => {
            try {
                console.log("req:payments:create");
                const {statusCode, message, data} = await apiPayments.Create({socio, amount});

                io.to(socket.id).emit("res:payments:create", {statusCode, message, data});

                const findAllPayments = await apiPayments.FindAll({});

                return io.to(socket.id).emit("res:payments:findAll", findAllPayments);
            } catch (e) {
                console.log(e);
            }
        });

        socket.on("req:payments:delete", async ({id}) => {
            try {
                console.log("req:payments:delete");
                const {statusCode, message, data} = await apiPayments.Delete({id});

                io.to(socket.id).emit("res:payments:delete", {statusCode, message, data});

                const findAllPayments = await apiPayments.FindAll({});

                return io.to(socket.id).emit("res:payments:findAll", findAllPayments);
            } catch (e) {
                console.log(e);
            }
        });

    });
});