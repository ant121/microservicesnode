const Controllers = require('../Controllers');
const {InternalError} = require("../settings");
const apiPartner = require('api-partner');

async function Create({socio, amount}){
    try{

        const validarSocios = await apiPartner.FindOne({id: socio});

        if(validarSocios.statusCode !== 200) return {statusCode: validarSocios.statusCode, data: validarSocios.data, message: validarSocios.message };

        if (!validarSocios.data.enable) return {statusCode: 400, data: null, message: 'El socio no esta habilitado para esta transaccion.' };

        let {statusCode, data, message} =  await Controllers.Create({socio, amount})

        return {statusCode, data, message }
    }catch (e) {
        console.log({step: 'service Create', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Delete({id}){
    try{

        const respFindOne = await Controllers.FindOne({where: {id}});

        if(respFindOne.statusCode !== 200) return {statusCode: respFindOne.statusCode, data: respFindOne.data, message: respFindOne.statusCode === 500 ? InternalError: respFindOne.message};

        let deleteUser =  await Controllers.Delete({where: {id} });

        return {statusCode: deleteUser.statusCode, data: respFindOne.data, message: deleteUser.message};

    }catch (e) {
        console.log({step: 'service Delete', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindOne({id}){
    try{

        let {statusCode, data, message} =  await Controllers.FindOne({where: {id}})

        return {statusCode, data, message}
    }catch (e) {
        console.log({step: 'service FindOne', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindAll({}){
    try{

        let {statusCode, data, message} =  await Controllers.FindAll({})

        return {statusCode, data, message}
    }catch (e) {
        console.log({step: 'service FindAll', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Create, Delete, FindOne, FindAll }

