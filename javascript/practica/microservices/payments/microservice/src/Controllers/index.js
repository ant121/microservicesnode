const {Model} = require("../Models");

async function Create({socio, amount}){
    try{
        let instance = await Model.create(
            {socio, amount},
            {fields: ['socio', 'amount'], logging: false});

        return {statusCode: 200, data: instance.toJSON(), message: null};
    }catch (e) {
        console.log({step: 'controller Create', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Delete({where = {} }){
    try{

        await Model.destroy({where, logging: false})

        return {statusCode: 200, data: 'ok', message: null};
    }catch (e) {
        console.log({step: 'controller Delete', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindOne({where = {}}){
    try{
        let instance = await Model.findOne({where, logging: false});

        if(instance!==null) return {statusCode: 200, data: instance.toJSON(), message: null}
        else return {statusCode: 400, data: null, message: "No existe el usuario"};

    }catch (e) {
        console.log({step: 'controller FindOne', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindAll(where = {} ){
    try{

        let instances = await Model.findAll({where, logging: false})

        return {statusCode: 200, data: instances, message: null};
    }catch (e) {
        console.log({step: 'controller FindAll', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Create, Delete, FindOne, FindAll}
