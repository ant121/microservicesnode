const Services = require('../Services');
const {InternalError} = require("../settings");
const {queueCreate,queueDelete,queueFindAll,queueFindOne} = require('./index');

async function FindAll (job, done) {

    try {
        const { } = job.data;

        let {statusCode, data, message} = await Services.FindAll({});

        console.log(job.id);

        done(null, {statusCode, data: data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindAll', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function FindOne (job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {id} = job.data;

        let {statusCode, data, message} = await Services.FindOne({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindOne', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }
}

async function Create(job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {socio, amount} = job.data;

        let {statusCode, data, message} = await Services.Create({socio, amount});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueCreate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Delete (job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {id} = job.data;

        let {statusCode, data, message} = await Services.Delete({id});

        console.log(data);
        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueDelete', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function run() {
    try{
        console.log('Iniciando worker...');
        queueFindAll.process(FindAll);
        queueFindOne.process(FindOne);
        queueCreate.process(Create);
        queueDelete.process(Delete);
        console.log('Worker iniciado');
    }catch (e) {
        console.log(e);
    }
}

module.exports = {FindAll, FindOne, Create, Delete, run}