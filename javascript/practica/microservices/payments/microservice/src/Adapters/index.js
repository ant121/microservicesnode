const bull = require('bull');
const {redis} = require('../settings');

const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull(`payments:create`, opts);
const queueDelete = bull(`payments:delete`, opts);
const queueFindOne = bull(`payments:findOne`, opts);
const queueFindAll = bull(`payments:findAll`, opts);

module.exports = {queueCreate, queueDelete, queueFindOne, queueFindAll};