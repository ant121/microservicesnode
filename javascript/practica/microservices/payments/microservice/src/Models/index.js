const {sequelize, name} = require('../settings');
const {DataTypes} = require('sequelize');

const Model = sequelize.define(name, {
    socio: {type: DataTypes.BIGINT},
    amount: {type: DataTypes.BIGINT}
});

const SyncDb = async () => {
    try {

        console.log('Iniciando base de datos...');

        //Sincronizacion de la base de datos
        await Model.sync({logging: false, force: true});

        console.log('Base de datos iniciada');

        return {statusCode: 200, data: 'ok', message: null};
    }catch (e) {
        console.log(e);

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Model, SyncDb}