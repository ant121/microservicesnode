### Api Pagos

La Api contiene funciones basicas para interactuar con pagos. Las funciones son realziadas en javascript. A continuacón,
se presenta un ejemplo de conexion mediante sockets.

Obtener todos los pagos

        socket.on("req:payments:findAll", async ({}) => {

            try {
                console.log("req:payments:findAll");
                const {statusCode, message, data} = await apiPayments.FindAll({});

                return io.to(socket.id).emit("res:payments:findAll", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }

        });

Buscar un pago por id

        socket.on("req:payments:findOne", async ({id}) => {
            try {
                console.log("req:payments:findOne");
                const {statusCode, message, data} = await apiPayments.FindOne({id});

                return io.to(socket.id).emit("res:payments:findOne", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Crear un pago. Datos. socio: 'identificador del socio', amount: 'Cantidad'

        socket.on("req:payments:create", async ({socio, amount}) => {
            try {
                console.log("req:payments:create");
                const {statusCode, message, data} = await apiPayments.Create({socio, amount});

                return io.to(socket.id).emit("res:payments:create", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Eliminar pago por id

        socket.on("req:payments:delete", async ({id}) => {
            try {
                console.log("req:payments:delete");
                const {statusCode, message, data} = await apiPayments.Delete({id});

                return io.to(socket.id).emit("res:payments:delete", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });
