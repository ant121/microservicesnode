const api = require('../api');

async function main(){
    try{
        const {statusCode, data, message} = await api.Create({title: 'Ready Player One', image: 'https://images-na.ssl-images-amazon.com/images/I/71BjAljTQeL.jpg'});

        console.log({statusCode, data, message});
    }catch (e) {
        console.error(e);
    }
}

main();