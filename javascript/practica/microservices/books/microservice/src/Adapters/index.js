const bull = require('bull');
const {redis} = require('../settings');

const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull(`book:create`, opts);
const queueDelete = bull(`book:delete`, opts);
const queueUpdate = bull(`book:update`, opts);
const queueFindOne = bull(`book:findOne`, opts);
const queueFindAll = bull(`book:findAll`, opts);

module.exports = {queueCreate, queueDelete, queueUpdate, queueFindOne, queueFindAll};
