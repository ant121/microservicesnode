const Services = require('../Services');
const {InternalError} = require("../settings");
const {queueCreate,queueDelete,queueUpdate,queueFindAll,queueFindOne} = require('./index');

async function FindAll (job, done) {

    try {

        const { } = job.data;

        let {statusCode, data, message} = await Services.FindAll({});

        done(null, {statusCode, data: data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindAll', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function FindOne (job, done) {

    try {

        const {title} = job.data;

        let {statusCode, data, message} = await Services.FindOne({title});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindOne', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Create(job, done) {

    try {
        const {title, image} = job.data;

        let {statusCode, data, message} = await Services.Create({title, image});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueCreate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }
}

async function Update (job, done) {

    try {
        const {title, category, sections, image, id} = job.data;

        let {statusCode, data, message} = await Services.Update({title, category, sections, image, id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueUpdate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Delete (job, done) {

    try {

        const {id} = job.data;

        let {statusCode, data, message} = await Services.Delete({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueDelete', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }
}

async function run() {
    try{
        console.log('Iniciando worker...');
        queueFindAll.process(FindAll);
        queueFindOne.process(FindOne);
        queueCreate.process(Create);
        queueUpdate.process(Update);
        queueDelete.process(Delete);
        console.log('Worker iniciado');
    }catch (e) {
        console.log(e);
    }
}

module.exports = {FindAll, FindOne, Create, Update, Delete, run}