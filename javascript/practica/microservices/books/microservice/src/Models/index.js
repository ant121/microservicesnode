const {sequelize, name} = require('../settings');
const {DataTypes} = require('sequelize');

const Model = sequelize.define(name, {
    title: {type: DataTypes.STRING},
    category: {type: DataTypes.BIGINT},
    sections: {type: DataTypes.STRING},
    image: {type: DataTypes.STRING}
});

const SyncDb = async () => {
    try {

        await Model.sync({logging: false, force: true});

        return {statusCode: 200, data: 'ok', message: null};
    }catch (e) {
        console.log(e);

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Model, SyncDb}