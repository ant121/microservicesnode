const micro = require('./src');

async function main(){

    try{
        //Sincronizacion de la abse de datos
        const respDb =await micro.SyncDb();

        //Verificar si ocurrio algun error
        if(respDb.statusCode!==200) throw respDb.message;

        //Ejecutar el microservicios
        await micro.run();
    }catch (e) {
        console.log(e);

        return {statusCode: 500, data: null, message: e.toString()};
    }

}

main();
