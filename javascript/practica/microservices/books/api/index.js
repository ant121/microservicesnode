const bull = require('bull');
const InternalError = 'No podemos procesar tu solicitud en estos momentos';

const redis = {
    host: '192.168.60.107',
    port: 6379
}

const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull(`book:create`, opts);
const queueDelete = bull(`book:delete`, opts);
const queueUpdate = bull(`book:update`, opts);
const queueFindOne = bull(`book:findOne`, opts);
const queueFindAll = bull(`book:findAll`, opts);

async function Create({title, image}) {

    try{

        const job = await queueCreate.add({title, image});

        const {statusCode, data, message} = await job.finished();

        if (statusCode === 200){
            console.log("Hola bienvenido", data.name);
        } else{
            console.log(message);
        }

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError Create:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Delete({id}) {

    try{

        const job = await queueDelete.add({id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};
    }catch (e) {

        console.log("ApiError Delete:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Update({title, category, sections, image, id}) {

    try{

        const job = await queueUpdate.add({title, category, sections, image, id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError Update:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function FindOne({title}) {

    try{

        const job = await queueFindOne.add({title});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError FindOne:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function FindAll({}) {

    try{

        const job = await queueFindAll.add({});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {
        console.log("ApiError FindAll:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

module.exports = {FindOne, FindAll, Create, Update, Delete};

// async function main(){
//       const x = await Create({title: 'Ready Player One', image: 'https://images-na.ssl-images-amazon.com/images/I/71BjAljTQeL.jpg'});
//       // await Delete({id:11});
//       // await FindAll({});
//       // await Update({age: 50, id: 1});
//       // await FindOne({id: 2}) ;
//     console.log(x);
// }
//
// main();
