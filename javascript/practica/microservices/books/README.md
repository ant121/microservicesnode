### Api Libros

La Api contiene funciones basicas para interactuar con libros. Las funciones son realziadas en javascript. A continuacón, 
se presenta un ejemplo de conexion mediante sockets.

Encontrar todos los libros

    socket.on("req:books:findAll", async ({}) => {

            try {
                console.log("req:books:findAll");
                const {statusCode, message, data} = await apiBooks.FindAll({});

                return io.to(socket.id).emit("res:books:findAll", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }

        });

Encontrar un libro por id

        socket.on("req:books:findOne", async ({id}) => {
            try {
                console.log("req:books:findOne");
                const {statusCode, message, data} = await apiBooks.FindOne({id});

                return io.to(socket.id).emit("res:books:findOne", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Crear un libro. Datos: title: 'Titulo del libro'

        socket.on("req:books:create", async ({title}) => {
            try {
                console.log("req:books:create");
                const {statusCode, message, data} = await apiBooks.Create({title});

                return io.to(socket.id).emit("res:books:create", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Actualizar un libro. Datos: title: 'Titulo del libro', category: 'Categoria del libro', sections: 'Donde se encuentra el libro', id: 'Identificador del libro'

        socket.on("req:books:update", async ({title, category, sections, id}) => {
            try {
                console.log("req:books:update");
                const {statusCode, message, data} = await apiBooks.Update({title, category, sections, id});
                return io.to(socket.id).emit("res:books:update", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Eliminar un libro por id

        socket.on("req:books:delete", async ({id}) => {
            try {
                console.log("req:books:delete");
                const {statusCode, message, data} = await apiBooks.Delete({id});

                return io.to(socket.id).emit("res:books:delete", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });
