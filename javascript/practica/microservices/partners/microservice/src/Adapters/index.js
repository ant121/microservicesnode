const bull = require('bull');
const {redis, name} = require('../settings');


const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull(`partner:create`, opts);
const queueDelete = bull(`partner:delete`, opts);
const queueUpdate = bull(`partner:update`, opts);
const queueFindOne = bull(`partner:findOne`, opts);
const queueFindAll = bull(`partner:findAll`, opts);
const queueEnable = bull(`partner:Enable`, opts);
const queueDisable = bull(`partner:Disable`, opts);


module.exports = {queueCreate, queueDelete, queueUpdate, queueFindOne, queueFindAll, queueEnable, queueDisable};
