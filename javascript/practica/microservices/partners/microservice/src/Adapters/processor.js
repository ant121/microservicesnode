const Services = require('../Services');
const {InternalError} = require("../settings");
const {
    queueCreate,
    queueDelete,
    queueUpdate,
    queueFindAll,
    queueFindOne,
    queueEnable,
    queueDisable
} = require('./index');

async function FindAll(job, done) {

    try {
        const {enable} = job.data;

        let {statusCode, data, message} = await Services.FindAll({enable});

        done(null, {statusCode, data: data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindAll', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }
}

async function FindOne(job, done) {

    try {
        const {id} = job.data;

        let {statusCode, data, message} = await Services.FindOne({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindOne', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Create(job, done) {

    try {
        const {name, phone} = job.data;

        let {statusCode, data, message} = await Services.Create({name, phone});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueCreate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Update(job, done) {

    try {
        const {name, age, email, phone, id} = job.data;

        let {statusCode, data, message} = await Services.Update({name, age, email, phone, id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueUpdate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Delete(job, done) {

    try {
        const {id} = job.data;

        let {statusCode, data, message} = await Services.Delete({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueDelete', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Enable(job, done) {

    try {
        const {id} = job.data;

        let {statusCode, data, message} = await Services.Enable({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueEnable', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}

async function Disable(job, done) {

    try {
        const {id} = job.data;

        let {statusCode, data, message} = await Services.Disable({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueDisable', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

}


async function run() {
    try {
        console.log('Iniciando worker...');
        queueFindAll.process(FindAll);
        queueFindOne.process(FindOne);
        queueCreate.process(Create);
        queueUpdate.process(Update);
        queueDelete.process(Delete);
        queueEnable.process(Enable);
        queueDisable.process(Disable);
        console.log('Worker iniciado');
    } catch (e) {
        console.log(e);
    }
}

module.exports = {FindAll, FindOne, Create, Update, Delete, Enable, Disable, run}