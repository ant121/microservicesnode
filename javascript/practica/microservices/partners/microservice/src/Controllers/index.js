const {Model} = require("../Models");

async function Create({name, phone}) {
    try {
        let instance = await Model.create(
            {name, phone},
            {fields: ['name', 'phone'], logging: false});

        return {statusCode: 200, data: instance.toJSON(), message: null};
    } catch (e) {
        console.log({step: 'controller Create', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Delete({where = {}}) {
    try {

        await Model.destroy({where, logging: false})

        return {statusCode: 200, data: 'ok', message: null};
    } catch (e) {
        console.log({step: 'controller Delete', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Update({name, age, email, phone, id}) {
    try {
        let instance = await Model.update(
            {name, age, email, phone},
            {where: {id}, logging: false, returning: true});

        return {statusCode: 200, data: instance[1][0].toJSON(), message: null};
    } catch (e) {
        console.log({step: 'controller Update', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindOne({where = {}}) {
    try {
        let instance = await Model.findOne({where, logging: false});

        if (instance !== null) return {statusCode: 200, data: instance.toJSON(), message: null}
        else return {statusCode: 400, data: null, message: "No existe el usuario"};

    } catch (e) {
        console.log({step: 'controller FindOne', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindAll({where = {}}) {
    try {

        let instances = await Model.findAll({where, logging: false})

        return {statusCode: 200, data: instances, message: null};
    } catch (e) {
        console.log({step: 'controller FindAll', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Enable({id}) {
    try {

        let instance = await Model.update(
            {enable: true},
            {where: {id}, logging: false, returning: true});

        return {statusCode: 200, data: instance[1][0].toJSON(), message: null};
    } catch (e) {
        console.log({step: 'controller Enable', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Disable({id}) {
    try {

        let instance = await Model.update(
            {enable: false},
            {where: {id}, logging: false, returning: true});

        return {statusCode: 200, data: instance[1][0].toJSON(), message: null};
    } catch (e) {
        console.log({step: 'controller Disable', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Create, Update, Delete, FindOne, FindAll, Enable, Disable}