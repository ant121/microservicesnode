const Controllers = require('../Controllers');
const {InternalError} = require("../settings");

async function Create({name, phone}) {
    try {

        let {statusCode, data, message} = await Controllers.Create({name, phone})

        return {statusCode, data, message}
    } catch (e) {
        console.log({step: 'service Create', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Delete({id}) {
    try {

        const respFindOne = await Controllers.FindOne({where: {id}});

        if (respFindOne.statusCode !== 200) return {
            statusCode: respFindOne.statusCode,
            data: respFindOne.data,
            message: respFindOne.statusCode === 500 ? InternalError : respFindOne.message
        };

        let deleteUser = await Controllers.Delete({where: {id}});

        return {statusCode: deleteUser.statusCode, data: respFindOne.data, message: deleteUser.message};

    } catch (e) {
        console.log({step: 'service Delete', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Update({name, age, email, phone, id}) {
    try {

        let {statusCode, data, message} = await Controllers.Update({name, age, email, phone, id})

        return {statusCode, data, message}
    } catch (e) {
        console.log({step: 'service Update', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindOne({id}) {
    try {

        let {statusCode, data, message} = await Controllers.FindOne({where: {id}})

        return {statusCode, data, message}
    } catch (e) {
        console.log({step: 'service FindOne', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindAll({enable}) {
    try {

        let where = {};

        if (enable !== undefined) where.enable = enable;

        let {statusCode, data, message} = await Controllers.FindAll({where});

        return {statusCode, data, message}
    } catch (e) {
        console.log({step: 'service FindAll', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Enable({id}) {
    try {

        const {statusCode, data, message} = await Controllers.Enable({id});

        return {statusCode, data, message};
    } catch (e) {
        console.log({step: 'service Enable', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Disable({id}) {
    try {

        const {statusCode, data, message} = await Controllers.Disable({id});

        return {statusCode, data, message};

    } catch (e) {
        console.log({step: 'service Disable', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Create, Delete, Update, FindOne, FindAll, Enable, Disable}

