const {sequelize, name} = require('../settings');
const {DataTypes} = require('sequelize');

const Model = sequelize.define(name, {
    name: {type: DataTypes.STRING},
    age: {type: DataTypes.BIGINT},
    email: {type: DataTypes.STRING},
    phone: {type: DataTypes.STRING},
    enable: {type: DataTypes.BOOLEAN, defaultValue: true}
}, {freezeTableName: true});

const SyncDb = async () => {
    try {

        await Model.sync({logging: false, force: true});

        return {statusCode: 200, data: 'ok', message: null};
    } catch (e) {
        console.log(e);

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Model, SyncDb}