const bull = require('bull');
const InternalError = 'No podemos procesar tu solicitud en estos momentos';

const redis = {
    host: '192.168.60.107',
    port: 6379
}

const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull(`partner:create`, opts);
const queueDelete = bull(`partner:delete`, opts);
const queueUpdate = bull(`partner:update`, opts);
const queueFindOne = bull(`partner:findOne`, opts);
const queueFindAll = bull(`partner:findAll`, opts);
const queueEnable = bull(`partner:Enable`, opts);
const queueDisable = bull(`partner:Disable`, opts);

async function Create({name, phone}) {

    try{

        const job = await queueCreate.add({name, phone});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError Create:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Delete({id}) {

    try{

        const job = await queueDelete.add({id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};
    }catch (e) {

        console.log("ApiError Delete:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Update({name, phone, email, age, id}) {

    try{

        const job = await queueUpdate.add({name, phone, email, age, id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError Update:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function FindOne({id}) {

    try{

        const job = await queueFindOne.add({id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError FindOne:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function FindAll({enable}) {

    try{

        const job = await queueFindAll.add({enable});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {
        console.log("ApiError FindAll:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Enable({id}) {

    try{

        const job = await queueEnable.add({id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {
        console.log("ApiError FindAll:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Disable({id}) {

    try{

        const job = await queueDisable.add({id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {
        console.log("ApiError FindAll:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

module.exports = {FindOne, FindAll, Create, Update, Delete, Enable, Disable};

