### Api Socios

La Api contiene funciones basicas para interactuar con socios. Las funciones son realziadas en javascript. A continuacón,
se presenta un ejemplo de conexion mediante sockets.


Encontrar todos los libros. Datos: enable: 'Filtrar por activo o desactivado'

        socket.on("req:partners:findAll", async ({enable}) => {

            try {
                console.log("req:partners:findAll");
                const {statusCode, message, data} = await apiPartners.FindAll({enable});

                return io.to(socket.id).emit("res:partners:findAll", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }

        });

Busqueda de socios por id

        socket.on("req:partners:findOne", async ({id}) => {
            try {
                console.log("req:partners:findOne");
                const {statusCode, message, data} = await apiPartners.FindOne({id});

                return io.to(socket.id).emit("res:partners:findOne", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Crear socios. Datos: name: 'Nombre del socio', phone: 'Telefono del socio'

        socket.on("req:partners:create", async ({name, phone}) => {
            try {
                console.log("req:partners:create");
                const {statusCode, message, data} = await apiPartners.Create({name, phone});

                return io.to(socket.id).emit("res:partners:create", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Crear socio. Datos: name: 'Nombre del socio', phone: 'Telefono del socio', email: 'Email del socio', age: 'Edad del socio', id: 'identificador del socio'

        socket.on("req:partners:update", async ({name, phone, email, age, id}) => {
            try {
                console.log("req:partners:update");
                const {statusCode, message, data} = await apiPartners.Update({name, phone, email, age, id});
                return io.to(socket.id).emit("res:partners:update", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Eliminar socio por id

        socket.on("req:partners:delete", async ({id}) => {
            try {
                console.log("req:partners:delete");
                const {statusCode, message, data} = await apiPartners.Delete({id});

                return io.to(socket.id).emit("res:partners:delete", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Activar usuarios por id

        socket.on("req:partners:enable", async ({id}) => {
            try {
                console.log("req:partners:enable");
                const {statusCode, message, data} = await apiPartners.Enable({id});

                return io.to(socket.id).emit("res:partners:enable", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });

Desactivar usuarios por id

        socket.on("req:partners:disable", async ({id}) => {
            try {
                console.log("req:partners:disable");
                const {statusCode, message, data} = await apiPartners.Disable({id});

                return io.to(socket.id).emit("res:partners:disable", {statusCode, message, data});
            } catch (e) {
                console.log(e);
            }
        });
