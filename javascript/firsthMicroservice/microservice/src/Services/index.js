const Controllers = require('../Controllers');
const {InternalError} = require("../settings");

async function Create({age, color, name}){
    try{

        let {statusCode, data, message} =  await Controllers.Create({age, color, name})

        return {statusCode, data, message }
    }catch (e) {
        console.log({step: 'service Create', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Delete({id}){
    try{

        const respFindOne = await Controllers.FindOne({where: {id}});

        if(respFindOne.statusCode !== 200) return {statusCode: respFindOne.statusCode, data: respFindOne.data, message: respFindOne.statusCode === 500 ? InternalError: respFindOne.message};

        let deleteUser =  await Controllers.Delete({where: {id} });

        return {statusCode: deleteUser.statusCode, data: respFindOne.data, message: deleteUser.message};

    }catch (e) {
        console.log({step: 'service Delete', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Update({age, color, name, id}){
    try{

        let {statusCode, data, message} =  await Controllers.Update({age, color, name, id})

        return {statusCode, data, message }
    }catch (e) {
        console.log({step: 'service Update', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindOne({id}){
    try{

        let {statusCode, data, message} =  await Controllers.FindOne({where: {id}})

        return {statusCode, data, message}
    }catch (e) {
        console.log({step: 'service FindOne', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindAll({}){
    try{

        let {statusCode, data, message} =  await Controllers.FindAll({})

        return {statusCode, data, message}
    }catch (e) {
        console.log({step: 'service FindAll', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Create, Delete, Update, FindOne, FindAll }

// async function Servicio({id}) {
//
//     try {
//
//         //Verifica si existe el usuairo
//         const existUser = await ExistUser({id});
//
//         if(existUser.statusCode !== 200) throw (existUser.message);
//
//         if(!existUser.data) throw ("No existe el usuario");
//         //---------------------------------------------
//
//         //Busca los datos del usuario
//         const findUser = await FindUser({id});
//
//         if(findUser.statusCode !== 200) throw (findUser.message);
//
//         if(findUser.data.info.age > 18){
//             console.log("Eres mayor de edad");
//         }
//
//         //Retorna los datos del usuario
//         return {statusCode: 200, data: findUser.data, message: null }
//     } catch (e) {
//
//         console.log({step: 'service Servicio', error: e.toString()})
//
//         return {statusCode: 500, data: null, message: e.toString()};
//     }
//
// }
