const bull = require('bull');
const {redis} = require('../settings');

const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull("curso:create", opts);
const queueDelete = bull("curso:delete", opts);
const queueUpdate = bull("curso:update", opts);
const queueFindOne = bull("curso:findOne", opts);
const queueFindAll = bull("curso:findAll", opts);

module.exports = {queueCreate, queueDelete, queueUpdate, queueFindOne, queueFindAll};







    // .process(async (job, done) => {
    //
    //     try {
    //         //Job. data es lo que nos envian a nosotros
    //         const {id} = job.data;
    //
    //         let {statusCode, data, message} = await Servicio({id});
    //
    //         done(null, {statusCode, data, message});
    //     } catch (e) {
    //
    //         console.log({step: 'adapter Adaptador', error: e.toString()})
    //
    //         done(null, {statusCode: 500, data: null, message: InternalError});
    //     }
    //
    //     // If the job throws an unhandled exception it is also handled correctly
    //     // throw new Error('some unexpected error');
    // })
