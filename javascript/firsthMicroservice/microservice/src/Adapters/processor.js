const Services = require('../Services');
const {InternalError} = require("../settings");
const {queueCreate,queueDelete,queueUpdate,queueFindAll,queueFindOne} = require('./index');

async function FindAll (job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const { } = job.data;

        let {statusCode, data, message} = await Services.FindAll({});

        console.log(job.id);

        done(null, {statusCode, data: data.map((v) => ({...v.toJSON(), worker: job.id })), message});
    } catch (e) {

        console.log({step: 'adapter queueFindAll', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

    // If the job throws an unhandled exception it is also handled correctly
    // throw new Error('some unexpected error');
}

async function FindOne (job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {id} = job.data;

        let {statusCode, data, message} = await Services.FindOne({id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueFindOne', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

    // If the job throws an unhandled exception it is also handled correctly
    // throw new Error('some unexpected error');
}

async function Create(job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {age, color, name} = job.data;

        let {statusCode, data, message} = await Services.Create({age, color, name});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueCreate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

    // If the job throws an unhandled exception it is also handled correctly
    // throw new Error('some unexpected error');
}

async function Update (job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {age,color,name,id} = job.data;

        let {statusCode, data, message} = await Services.Update({age,color,name,id});

        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueUpdate', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

    // If the job throws an unhandled exception it is also handled correctly
    // throw new Error('some unexpected error');
}

async function Delete (job, done) {

    try {
        //Job. data es lo que nos envian a nosotros
        const {id} = job.data;

        let {statusCode, data, message} = await Services.Delete({id});

        console.log(data);
        done(null, {statusCode, data, message});
    } catch (e) {

        console.log({step: 'adapter queueDelete', error: e.toString()})

        done(null, {statusCode: 500, data: null, message: InternalError});
    }

    // If the job throws an unhandled exception it is also handled correctly
    // throw new Error('some unexpected error');
}

async function run() {
    try{
        console.log('Iniciando worker...');
        queueFindAll.process(FindAll);
        queueFindOne.process(FindOne);
        queueCreate.process(Create);
        queueUpdate.process(Update);
        queueDelete.process(Delete);
        console.log('Worker iniciado');
    }catch (e) {
        console.log(e);
    }
}

module.exports = {FindAll, FindOne, Create, Update, Delete, run}