const {Model} = require("../Models");

async function Create({name, age, color}){
    try{
        let instance = await Model.create(
            {name, age, color},
            {fields: ['name', 'age', 'color'], logging: false});

        return {statusCode: 200, data: instance.toJSON(), message: null};
    }catch (e) {
        console.log({step: 'controller Create', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Delete({where = {} }){
    try{

        await Model.destroy({where, logging: false})

        return {statusCode: 200, data: 'ok', message: null};
    }catch (e) {
        console.log({step: 'controller Delete', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function Update({name, age, color, id}){
    try{
        let instance = await Model.update(
            {name, age, color},
            { where: {id}, logging: false, returning: true });

        return {statusCode: 200, data: instance[1][0].toJSON(), message: null};
    }catch (e) {
        console.log({step: 'controller Update', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindOne({where = {}}){
    try{
        let instance = await Model.findOne({where, logging: false});

        if(instance!==null) return {statusCode: 200, data: instance.toJSON(), message: null}
        else return {statusCode: 400, data: null, message: "No existe el usuario"};

    }catch (e) {
        console.log({step: 'controller FindOne', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

async function FindAll(where = {} ){
    try{

        let instances = await Model.findAll({where, logging: false})

        return {statusCode: 200, data: instances, message: null};
    }catch (e) {
        console.log({step: 'controller FindAll', error: e.toString()})

        return {statusCode: 500, data: null, message: e.toString()};
    }
}

module.exports = {Create, Update, Delete, FindOne, FindAll}

// async function FindUser({id}) {
//
//     try {
//
//         const user = db.filter(x => x.id === id)[0];
//
//         console.log(user);
//
//         await setTimeout(1000);
//
//         return {statusCode: 200, data: user, message: null};
//
//     } catch (e) {
//         console.log({step: 'controller FindUser', error: e.toString()})
//
//         return {statusCode: 500, data: null, message: e.toString()};
//     }
//
// }
//
// async function ExistUser({id}) {
//
//     try {
//
//         const match = db.some(x => x.id === id);
//
//         return {statusCode: 200, data: match, message: null};
//     } catch (e) {
//         console.log({step: 'controller ExistUser', error: e.toString()})
//
//         return {statusCode: 500, data: null, message: e.toString()};
//     }
//
// }

// function PreferencesColor({color}) {
//
//     let c;
//
//     try {
//         if (color === 'rojo') c = 'azul';
//
//         if (color === 'blanco') c = 'negro';
//
//         if (color === 'azul') c = 'rojo';
//
//         return {statusCode: 200, data: c, message: null};
//     } catch (e) {
//         console.log({step: 'controller PreferencesColor', error: e.toString()})
//
//         return {statusCode: 500, data: null, message: e.toString()};
//     }
//
// }
