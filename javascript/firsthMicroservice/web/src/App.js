import React, {useEffect, useState} from "react";

const {io} = require('socket.io-client');
const socket = io("http://localhost", {
    port: 80,
    transports: ['websocket'],
    jsonp: false
});

function App() {
    const [id, setId] = useState();
    const [data, setData] = useState([]);

    useEffect(() => {
        setTimeout(() => setId(socket.id), 200);

        socket.on('res:microservice:findAll', ({statusCode, data, message}) => {
            console.log('res:microservice:findAll', {statusCode, data, message});
            if(statusCode===200){
                setData(data);
            }
        });

        socket.on('res:microservice:create', ({statusCode, data, message}) => {
            console.log('res:microservice:create', {statusCode, data, message});
        });

        socket.on('res:microservice:findOne', ({statusCode, data, message}) => {
            console.log('res:microservice:findOne', {statusCode, data, message});
        });

        socket.on('res:microservice:update', ({statusCode, data, message}) => {
            console.log('res:microservice:update', {statusCode, data, message});
        });
        socket.on('res:microservice:delete', ({statusCode, data, message}) => {
            console.log('res:microservice:delete', {statusCode, data, message});
        });

        setInterval(() => {
          socket.emit('req:microservice:findAll', ({}));
        }, 1500)

        // socket.emit('req:microservice:findAll', ({}));

    }, [])

    return (
        <div>
            <header>
                <h1>{id ? `Estas en linea ${id}` : 'Fuera de linea'}</h1>
                {data.map((data, index) => {
                    return <p key={index}>Nombre: {data.name}, Edad: {data.age} {data.worker}</p>
                })}
            </header>
        </div>
    );
}

export default App;
