const express = require('express');
const http = require('http');
const {Server} = require('socket.io');

const app = express();
const server = http.createServer(app);

const io = new Server(server);

const api = require('../api/index');

server.listen(80, ()=>{
    console.log("Server initialize");
    io.on('connection', socket => {
        console.log("New Conection", socket.id);

        socket.on("req:microservice:findAll", async ({ })=> {

            try{
                console.log("req:microservice:findAll");
                const {statusCode, message, data} = await api.FindAll({});

                return io.to(socket.id).emit("res:microservice:findAll", {statusCode, message, data});
            }catch (e) {
                console.log(e);
            }

        });

        socket.on("req:microservice:findOne", async ({id})=>{
            try{
                console.log("req:microservice:findOne");
                const {statusCode, message, data} = await api.FindOne({id});

                return io.to(socket.id).emit("res:microservice:findOne", {statusCode, message, data});
            }catch (e) {
                console.log(e);
            }
        });

        socket.on("req:microservice:create", async ({age, color, name})=>{
            try{
                console.log("req:microservice:create");
                const {statusCode, message, data} = await api.Create({age, color, name});

                return io.to(socket.id).emit("res:microservice:create", {statusCode, message, data});
            }catch (e) {
                console.log(e);
            }
        });

        socket.on("req:microservice:update", async({age, name, color, id})=>{
            try{
                console.log("req:microservice:update");
                const {statusCode, message, data} = await api.Update({age, name, color, id});
                return io.to(socket.id).emit("res:microservice:update", {statusCode, message, data});
            }catch (e) {
                console.log(e);
            }
        });

        socket.on("req:microservice:delete", async({id})=>{
            try{
                console.log("req:microservice:delete");
                const {statusCode, message, data} = await api.Delete({id});

                return io.to(socket.id).emit("res:microservice:delete", {statusCode, message, data});
            }catch (e) {
                console.log(e);
            }
        });

    });
});