const {io} = require('socket.io-client');

const socket = io("http://localhost", {port: 80});

async function main() {

    try {
        setTimeout(() => console.log(socket.id), 500);

        socket.on('res:microservice:findAll', ({statusCode, data, message}) => {
            console.log('res:microservice:findAll', {statusCode, data, message});
        });

        socket.on('res:microservice:create', ({statusCode, data, message}) => {
            console.log('res:microservice:create', {statusCode, data, message});
        });

        socket.on('res:microservice:findOne', ({statusCode, data, message}) => {
            console.log('res:microservice:findOne', {statusCode, data, message});
        });

        socket.on('res:microservice:update', ({statusCode, data, message}) => {
            console.log('res:microservice:update', {statusCode, data, message});
        });
        socket.on('res:microservice:delete', ({statusCode, data, message}) => {
            console.log('res:microservice:delete', {statusCode, data, message});
        });

        // setInterval(() => {
        //     socket.emit('req:microservice:findAll', ({}));
        // }, 500)

        setTimeout(() => {
           socket.emit('req:microservice:create', ({name: "Elvis Perez", age: 50, color: "blue"}));
           // socket.emit('req:microservice:findAll', ({}));
           //  socket.emit('req:microservice:delete', ({id: 1}));
           //  socket.emit('req:microservice:update', ({color: "blue", id: 3}))
           //  socket.emit('req:microservice:findOne', ({id: 3}));
        }, 300);

    } catch (e) {
        console.log("clientError:",e);
    }
}

main();