const bull = require('bull');
const {InternalError} = require("../microservice/src/settings");

const redis = {
    host: '192.168.1.32',
    port: 6379
}

const opts = { redis: {host: redis.host, port: redis.port}};

const queueCreate = bull("curso:create", opts);
const queueDelete = bull("curso:delete", opts);
const queueUpdate = bull("curso:update", opts);
const queueFindOne = bull("curso:findOne", opts);
const queueFindAll = bull("curso:findAll", opts);

async function Create({age, color, name}) {

    try{

        const job = await queueCreate.add({age, color, name});

        const {statusCode, data, message} = await job.finished();

        // if (statusCode === 200){
        //     console.log("Hola bienvenido", data.name);
        // } else{
        //     console.log(message);
        // }

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError Create:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Delete({id}) {

    try{

        const job = await queueDelete.add({id});

        const {statusCode, data, message} = await job.finished();

        // if (statusCode === 200){
        //     console.log("Se ha eliminado el usuario:", data.name);
        // } else{
        //     console.log(message);
        // }

        return {statusCode, data, message};
    }catch (e) {

        console.log("ApiError Delete:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function Update({age, name, color, id}) {

    try{

        const job = await queueUpdate.add({age, name, color, id});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

        // if (statusCode === 200){
        //     console.log("Hola bienvenido ", data.name);
        // } else{
        //     console.log(message);
        // }

    }catch (e) {

        console.log("ApiError Update:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function FindOne({id}) {

    try{

        const job = await queueFindOne.add({id});

        const {statusCode, data, message} = await job.finished();

        // if (statusCode === 200){
        //     return {statusCode, data, message};
        // } else{
        //     console.log(message);
        // }

        return {statusCode, data, message};

    }catch (e) {

        console.log("ApiError FindOne:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

async function FindAll({}) {

    try{

        const job = await queueFindAll.add({});

        const {statusCode, data, message} = await job.finished();

        return {statusCode, data, message};

    }catch (e) {
        console.log("ApiError FindAll:", e);

        return {statusCode: 500, data: null, message: InternalError};
    }

}

module.exports = {FindOne, FindAll, Create, Update, Delete};

// async function main(){
//       // await Create({name:"Elvis Perez", age: 23, color: "blue"});
//       // await Delete({id:11});
//       // await FindAll({});
//       // await Update({age: 50, id: 1});
//       await FindOne({id: 2}) ;
// }
//
// main();
